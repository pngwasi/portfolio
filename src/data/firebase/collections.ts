export enum FirebaseCollections {
  PROJECTS = "projects",
  SKILLS = "skills",
  RESUME = "resume",
}

export enum FirebaseFilePaths {
  PROJECTS = "portfolio-projects",
  SKILLS = "portfolio-skills",
  RESUME = "portfolio-resume",
}
