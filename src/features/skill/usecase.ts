import { SkillRepository } from "./repository";

export function getSkills(repo: SkillRepository) {
  return repo.getSkills();
}
